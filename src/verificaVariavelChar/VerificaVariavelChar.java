package verificaVariavelChar;

public class VerificaVariavelChar {

	public static void main(String[] args) {
		
		/* CHAR representa um character */
		char pessoa = 'F';
		
		if(pessoa == 'F') {
			System.out.println("Pessoa Fisica");
		}else {
			System.out.println("Pessoa Juridica");
		}
		
	}
}
